package com.example.sensormanagementapi.service;

import com.example.sensormanagementapi.dto.request.AddSensorRequest;
import com.example.sensormanagementapi.dto.response.SensorDTO;
import com.example.sensormanagementapi.message.Message;
import com.example.sensormanagementapi.repository.SensorRepository;
import io.vavr.control.Either;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Optional;
import java.util.UUID;

public class SensorServiceTest {
    private final SensorRepository sensorRepository = new SensorRepository();
    private final SensorService sensorService = new SensorService(sensorRepository);

    @Test
    public void add_returnUser_dataAreCorrect(){
        //given
        String correctAddress = "Długa 43A, 12-545 Kazimierz Dolny, Polska";
        String correctOwner = "Anna Kowalska";
        AddSensorRequest addSensorRequest = new AddSensorRequest(correctAddress, correctOwner);

        //when
        Either<Message, SensorDTO> result = sensorService.add(addSensorRequest);

        //then
        Assertions.assertThat(result.isRight()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "Długa 43A, 12-545 Kazimierz Dolny", // country is empty
            "Długa, 12-545 Kazimierz Dolny, Polska", // street number is empty
            "Długa 22, Kazimierz Dolny, Polska", // post code is empty
            "Długa 22, 3-22 Kazimierz Dolny, Polska", // post code is incorrect
    })
    public void add_returnError_addressIsIncorrect(String incorrectAddress){
        //given
        String correctOwner = "Anna Kowalska";
        AddSensorRequest addSensorRequest = new AddSensorRequest(incorrectAddress, correctOwner);

        //when
        Either<Message, SensorDTO> result = sensorService.add(addSensorRequest);

        //then
        Assertions.assertThat(result.isLeft()).isTrue();
    }

    @Test
    public void add_returnError_ownerIsIncorrect(){
        //given
        String correctAddress = "Długa 43A, 12-545 Kazimierz Dolny, Polska";
        String incorrectOwner = "AnnaKowalska";
        AddSensorRequest addSensorRequest = new AddSensorRequest(correctAddress, incorrectOwner);

        //when
        Either<Message, SensorDTO> result = sensorService.add(addSensorRequest);

        //then
        Assertions.assertThat(result.isLeft()).isTrue();
    }

    @Test
    public void getOne_isPresent_userExist(){
        //given
        String address = "Długa 43A, 12-545 Kazimierz Dolny, Polska";
        String owner = "Anna Kowalska";
        AddSensorRequest addSensorRequest = new AddSensorRequest(address, owner);
        SensorDTO existedSensor = sensorService.add(addSensorRequest).get();

        //when
        Optional<SensorDTO> sensor = sensorService.getOne(existedSensor.id);

        //then
        Assertions.assertThat(sensor.isPresent()).isTrue();
    }

    @Test
    public void getOne_isEmpty_userDoesNotExist(){
        //given
        UUID sensorId = UUID.randomUUID();

        //when
        Optional<SensorDTO> sensor = sensorService.getOne(sensorId);

        //then
        Assertions.assertThat(sensor.isEmpty()).isTrue();
    }

    @Test
    public void delete_true_userExist(){
        //given
        String correctAddress = "Długa 43A, 12-545 Kazimierz Dolny, Polska";
        String incorrectOwner = "Anna Kowalska";
        AddSensorRequest addSensorRequest = new AddSensorRequest(correctAddress, incorrectOwner);
        SensorDTO existedSensor = sensorService.add(addSensorRequest).get();

        //when
        boolean deleted = sensorService.delete(existedSensor.id);

        //then
        Assertions.assertThat(deleted).isTrue();
    }

    @Test
    public void delete_false_userDoesNotExist(){
        //given
        UUID sensorId = UUID.randomUUID();

        //when
        boolean deleted = sensorService.delete(sensorId);

        //then
        Assertions.assertThat(deleted).isFalse();
    }

}
