package com.example.sensormanagementapi.dto.response;

import java.io.Serializable;
import java.util.UUID;

public class SensorDTO implements Serializable {
    
    public static final class OwnerDTO{
        public final String name;
        public final String surname;

        public OwnerDTO(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }
    }

    public static final class AddressDTO{
        public final String street;
        public final String streetNumber;
        public final String postCode;
        public final String city;
        public final String country;

        public AddressDTO(String street, String streetNumber, String postCode, String city, String country) {
            this.street = street;
            this.streetNumber = streetNumber;
            this.postCode = postCode;
            this.city = city;
            this.country = country;
        }
    }
    
    public final UUID id;
    public final OwnerDTO owner;
    public final AddressDTO address;

    public SensorDTO(UUID id, OwnerDTO ownerDTO, AddressDTO addressDTO) {
        this.id = id;
        this.owner = ownerDTO;
        this.address = addressDTO;
    }
    
}
