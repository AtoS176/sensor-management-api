package com.example.sensormanagementapi.controller;

import com.example.sensormanagementapi.dto.response.SensorDTO;
import com.example.sensormanagementapi.dto.request.AddSensorRequest;
import com.example.sensormanagementapi.message.Message;
import com.example.sensormanagementapi.service.SensorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/sensors")
public class SensorController {
    private final SensorService sensorService;

    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable UUID id) {
        Optional<SensorDTO> sensorDTO = sensorService.getOne(id);

        return sensorDTO.isPresent() ?
                ResponseEntity.ok(sensorDTO) :
                ResponseEntity.badRequest().body(Message.INCORRECT_SENSOR_UUID.getMessage());
    }

    @GetMapping()
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(sensorService.getAll());
    }

    @PostMapping()
    public ResponseEntity<?> add(@RequestBody AddSensorRequest addSensorRequest) {
        return sensorService.add(addSensorRequest)
                .fold(error -> ResponseEntity.badRequest().body(error.getMessage()), ResponseEntity::ok);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        return sensorService.delete(id) ?
                ResponseEntity.status(HttpStatus.NO_CONTENT).build() :
                ResponseEntity.badRequest().body(Message.INCORRECT_SENSOR_UUID.getMessage());
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable UUID id, @RequestBody AddSensorRequest addSensorRequest) {
        return sensorService.update(id, addSensorRequest)
                .fold(error -> ResponseEntity.badRequest().body(error.getMessage()), ResponseEntity::ok);
    }

}
