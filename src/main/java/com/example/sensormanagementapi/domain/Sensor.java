package com.example.sensormanagementapi.domain;

import com.example.sensormanagementapi.dto.response.SensorDTO;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Sensor {
    private final static Set<UUID> ID_SET = new HashSet<>();

    private final UUID id;
    private final Owner owner;
    private final Address address;

    private Sensor(UUID id, Owner owner, Address address) {
        this.id = id;
        this.owner = owner;
        this.address = address;
    }

    public static Sensor createOf(Owner owner, Address address){
        return new Sensor(generateId(), owner, address);
    }

    private static synchronized UUID generateId(){
        UUID id = UUID.randomUUID();
        while(ID_SET.contains(id)){
            id = UUID.randomUUID();
        }
        ID_SET.add(id);
        return id;
    }

    public UUID getId() {
        return id;
    }

    public SensorDTO createDTO(){
        return new SensorDTO(id, owner.createDTO(), address.createDTO());
    }

    public Sensor update(Owner owner, Address address){
        return new Sensor(id, owner, address);
    }

}
