package com.example.sensormanagementapi.service;

import com.example.sensormanagementapi.domain.Address;
import com.example.sensormanagementapi.domain.Owner;
import com.example.sensormanagementapi.domain.Sensor;
import com.example.sensormanagementapi.dto.response.SensorDTO;
import com.example.sensormanagementapi.dto.request.AddSensorRequest;
import com.example.sensormanagementapi.message.Message;
import com.example.sensormanagementapi.repository.SensorRepository;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SensorService {
    private final SensorRepository sensorRepository;

    public SensorService(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    public Either<Message, SensorDTO> add(AddSensorRequest addSensorRequest){
        return createOwnerAndAddress(addSensorRequest).fold(
                Either::left,
                success -> {
                    Sensor sensor = Sensor.createOf(success._1(), success._2());
                    sensorRepository.add(sensor);
                    return Either.right(sensor.createDTO());
                }) ;
    }

    public Either<Message, SensorDTO> update(UUID id, AddSensorRequest addSensorRequest){
        Optional<Sensor> sensor = sensorRepository.getOne(id);

        if(sensor.isEmpty()){
            return Either.left(Message.INCORRECT_SENSOR_UUID);
        }

        return createOwnerAndAddress(addSensorRequest).fold(
                Either::left,
                success -> {
                    Sensor updated = sensor.get().update(success._1(), success._2());
                    sensorRepository.add(updated);
                    return Either.right(updated.createDTO());
                }
        );
    }

    public Optional<SensorDTO> getOne(UUID id){
        return sensorRepository.getOne(id).map(Sensor::createDTO);
    }

    public List<SensorDTO> getAll(){
        return sensorRepository.getAll()
                .stream()
                .map(Sensor::createDTO)
                .collect(Collectors.toList());
    }

    public boolean delete(UUID id){
        return sensorRepository.delete(id);
    }

    private Either<Message, Tuple2<Owner, Address>> createOwnerAndAddress(AddSensorRequest addSensorRequest){
        if(addSensorRequest.getAddress() == null || addSensorRequest.getOwner() == null){
            return Either.left(Message.INCORRECT_ARGS_NUMBER);
        }

        Try<Owner> owner = Try.of(() -> Owner.createOf(addSensorRequest.getOwner()));
        Try<Address> address = Try.of(() -> Address.createOf(addSensorRequest.getAddress()));

        if(owner.isFailure()){
            return Either.left(Message.INCORRECT_OWNER);
        }

        if(address.isFailure()){
            return Either.left(Message.INCORRECT_ADDRESS);
        }

        return Either.right(Tuple.of(owner.get(), address.get()));
    }

}
