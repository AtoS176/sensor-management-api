package com.example.sensormanagementapi.repository;

import com.example.sensormanagementapi.domain.Sensor;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class SensorRepository {
    private final Map<UUID, Sensor> sensors = new ConcurrentHashMap<>();

    public Sensor add(Sensor sensor){
        sensors.put(sensor.getId(), sensor);
        return sensor;
    }

    public boolean delete(UUID id){
        if(!sensors.containsKey(id)){
            return false;
        }
        sensors.remove(id);
        return true;
    }

    public Optional<Sensor> getOne(UUID id){
        if(sensors.containsKey(id)){
            return Optional.of(sensors.get(id));
        }
        return Optional.empty();
    }

    public List<Sensor> getAll(){
        return new ArrayList<>(sensors.values());
    }

}
