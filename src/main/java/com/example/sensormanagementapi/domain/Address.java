package com.example.sensormanagementapi.domain;

import com.example.sensormanagementapi.dto.response.SensorDTO;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Address implements Serializable {
    private final static Pattern POST_CODE_REGEX = Pattern.compile("^[0-9]{2}-[0-9]{3}");
    private final String street;
    private final String streetNumber;
    private final String postCode;
    private final String city;
    private final String country;

    private Address(String street, String streetNumber, String postCode, String city, String country) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public static Address createOf(String address){
        String[] addressElements = address.split(", ");

        //Validation of address
        if(addressElements.length != 3){
            throw new IllegalArgumentException("Incorrect address");
        }

        String streetWithNumber = addressElements[0];
        String cityWithPostCode = addressElements[1];
        String country = addressElements[2];

        String[] streetDetails = streetWithNumber.split(" ");
        String streetNumber = streetDetails[streetDetails.length-1];
        String streetName = streetWithNumber.replace(" " + streetNumber, "");

        //Validation of street address
        if(streetDetails.length < 2 || !Character.isDigit(streetNumber.charAt(0))){
            throw new IllegalArgumentException("Incorrect street address");
        }

        String[] cityDetails = cityWithPostCode.split(" ");
        String postCode = cityDetails[0];
        String city = cityWithPostCode.replace(postCode + " ", "");

        //Validation of city address
        Matcher postCodeMatcher = POST_CODE_REGEX.matcher(postCode);
        if(cityDetails.length < 2 || !postCodeMatcher.find()){
            throw new IllegalArgumentException("Incorrect city address");
        }

        return new Address(streetName, streetNumber, postCode, city, country);
    }

    public SensorDTO.AddressDTO createDTO(){
        return new SensorDTO.AddressDTO(street, streetNumber, postCode, city, country);
    }

}
