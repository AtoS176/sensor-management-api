package com.example.sensormanagementapi.message;

public enum Message {
    ERROR("Wystąpił problem."),
    WHITESPACE("Proszę, zwróć uwagę na białe znaki."),
    INCORRECT_OWNER("Błędna nazwa właściciela. Nazwa powinna mieć format : Imie Nazwisko ."),
    INCORRECT_ADDRESS("Błędnie podany adres. Adres powinien mieć format : Ulica Numer_Ulicy, Kod_Pocztowy Miasto, Kraj ."),
    INCORRECT_ARGS_NUMBER("Podałeś za mało parametrów. Właściciel oraz Adres są polami obowiązkowymi."),
    INCORRECT_SENSOR_UUID("Podałeś błędny ID, czujnik o takim ID nie istnieje.");

    private final String msg;

    Message(String msg) {
        this.msg = msg;
    }

    public String getMessage(){
        return ERROR.msg + this.msg + WHITESPACE.msg;
    }

}
