package com.example.sensormanagementapi.dto.request;

import java.io.Serializable;

public class AddSensorRequest implements Serializable {
    private final String address;
    private final String owner;

    public AddSensorRequest(String address, String owner) {
        this.address = address;
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public String getOwner() {
        return owner;
    }

}
