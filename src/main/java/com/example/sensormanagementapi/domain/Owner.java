package com.example.sensormanagementapi.domain;

import com.example.sensormanagementapi.dto.response.SensorDTO;

public class Owner {
    private final String name;
    private final String surname;

   private Owner(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public static Owner createOf(String owner){
       String[] ownerDetails = owner.split(" ");

       if(ownerDetails.length != 2){
           throw new IllegalArgumentException("Incorrect owner.");
       }

       return new Owner(ownerDetails[0], ownerDetails[1]);
    }

    public SensorDTO.OwnerDTO createDTO(){
       return new SensorDTO.OwnerDTO(name, surname);
    }

}
