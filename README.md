<h3>1.Uruchomienie Aplikacji.</h3>

Aplikacja może zostać uruchomiana na lokalnym komputerze lub w kontenerze Docker. <br>
Po uruchomieniu aplikacja będzie działać na porcie 8080.

<ul>
<li>Sklonuj repozytorium : git clone https://gitlab.com/AtoS176/sensor-management-api</li>
<li>Jeśli chcesz uruchomić aplikację lokalnie (wymagane JDK 11+)
<ul><li>uruchom polecenie : gradle bootRun</li></ul>
<li>Jeśli chcesz uruchomić aplikację w kontenerze Docker
<ul>
<li>sudo docker build -f api.Dockerfile -t sensor_api_lp .</li>
<li>sudo docker run -p 8080:8080 sensor_api_lp</li>
</ul>
</li>
</ul>
  
<h3>2.Dokumentacja API</h3>
Dokumentacja została stworzona przy pomocy narzędzia POSTMAN.
Oprócz endpointów są tam również widoczne przykładowe odpowiedzi. <br><br>
Link : https://documenter.getpostman.com/view/11132181/TzRLmqzz

<h3>3. Kilka komentarzy</h3>

<p>
W dokumentacji było napisane, że zarówno właścicel jak i adres są przechowywane w postaci tekstu.
API oczekuje, że dane zostaną dostarczone właśnie w takiej postaci,
natomiast po stronie serwera informację te są przechowywane w postaci obiektów, które posiadają odpowiednie pola.
Wybrałem taki rodzaj przechowywania informacji, ponieważ trzymanie adresu jako jeden duży String nie wydaję się być
zbyt elastycznym podejściem. Wobec tego każdy obiekt jest walidowany, dlatego aby otrzymać od serwera odpowiedz
z rodziny 200, należy podać prawidłowy adres oraz właścieicla (schemat zgodny z dokumentacją).
W przypadku błędego formatu adresu/właściciela otrzymujemy odpowiedz z kodem 400 + komunikat o napotkanym błędzie,
również takie przykłady znajdują się w dokumentacji. Przykłady kilku nieporawnych adresów można również znalezć
w klasie SensorServiceTest.
</p>
